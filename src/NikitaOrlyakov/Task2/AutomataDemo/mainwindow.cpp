#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QPixmap pix;
    pix.load("./../AutomataDemo/background.jpg");  // ./ is catalog build-AutomataDemo-Debug/ !! he is must be in the same folder that AutomataDemo/
    pix = pix.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette pal1;
    pal1.setBrush(QPalette::Background, pix);
    this->setPalette(pal1);
    QPalette pal2;
    pal2.setColor(QPalette::WindowText, Qt::white);
    ui->label->setPalette(pal2);
    ui->label_2->setPalette(pal2);
    ui->label_3->setPalette(pal2);
    ui->label_4->setPalette(pal2);
    ui->label_5->setPalette(pal2);
    ui->label_6->setPalette(pal2);
    ui->label_7->setPalette(pal2);
    ui->label_8->setPalette(pal2);
    ui->label_9->setPalette(pal2);
    ui->progressBar->setValue(0);

    connect(&timer, SIGNAL(timeout()), this, SLOT(on_Cooking()));

    coffee_machine = new Automata("./../AutomataDemo/menu.xml");
    coffee_machine->on();
    std::map <std::string, int> menu = coffee_machine->print_menu();

    QLineEdit* DrinkNames[16] = {ui->leDrinkName1, ui->leDrinkName2, ui->leDrinkName3, ui->leDrinkName4,
                                ui->leDrinkName5, ui->leDrinkName6, ui->leDrinkName7, ui->leDrinkName8,
                                ui->leDrinkName9, ui->leDrinkName10, ui->leDrinkName11, ui->leDrinkName12,
                                ui->leDrinkName13, ui->leDrinkName14, ui->leDrinkName15, ui->leDrinkName16};

    QLineEdit* Prices[16] = {ui->lePrice1, ui->lePrice2, ui->lePrice3, ui->lePrice4, ui->lePrice5, ui->lePrice6,
                             ui->lePrice7, ui->lePrice8, ui->lePrice9, ui->lePrice10, ui->lePrice11, ui->lePrice12,
                             ui->lePrice13, ui->lePrice14, ui->lePrice15, ui->lePrice16};

    QPushButton* ChoiceButtons[16]= {ui->pbChoiceDrink1, ui->pbChoiceDrink2, ui->pbChoiceDrink3,
                                     ui->pbChoiceDrink4, ui->pbChoiceDrink5, ui->pbChoiceDrink6,
                                     ui->pbChoiceDrink7, ui->pbChoiceDrink8, ui->pbChoiceDrink9,
                                     ui->pbChoiceDrink10, ui->pbChoiceDrink11, ui->pbChoiceDrink12,
                                     ui->pbChoiceDrink13, ui->pbChoiceDrink14, ui->pbChoiceDrink15,
                                     ui->pbChoiceDrink16};

    for (int i=0; i<16; ++i)
    {
        if (!menu.empty())
        {
        DrinkNames[i]->setText(QString::fromStdString(menu.begin()->first));
        Prices[i]->setText(QString::number(menu.begin()->second));
        menu.erase(menu.begin());
        }
        else ChoiceButtons[i]->setEnabled(false);
    }
    ui->teInformation->setText("Hello");
    ui->teInformation->setText("Enter the cash");
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pbEnterCash_clicked()
{
    coffee_machine->coin(ui->leEnterCash->text().toInt());
    ui->teInformation->setText("You entered:");
    ui->teInformation->append(QString::number(coffee_machine->get_cash()));
    ui->teInformation->append("Choose the drink");
    ui->leEnterCash->setText("");
}


void MainWindow::choice_drink(const QString& drinkname)
{
    coffee_machine->choice();
    ui->teInformation->setText("Your choice:");
    ui->teInformation->append(drinkname);
    int tmp = coffee_machine->check(drinkname.toStdString());
    if (tmp > 0)
    {
        ui->teInformation->append("Don't enought:");
        ui->teInformation->append(QString::number(tmp));
    }
   if (tmp == 0)
   {
       coffee_machine->cook(drinkname.toStdString());
       ui->teInformation->append("Please wait.");

       QPushButton* ChoiceButtons[16]= {ui->pbChoiceDrink1, ui->pbChoiceDrink2, ui->pbChoiceDrink3,
                                        ui->pbChoiceDrink4, ui->pbChoiceDrink5, ui->pbChoiceDrink6,
                                        ui->pbChoiceDrink7, ui->pbChoiceDrink8, ui->pbChoiceDrink9,
                                        ui->pbChoiceDrink10, ui->pbChoiceDrink11, ui->pbChoiceDrink12,
                                        ui->pbChoiceDrink13, ui->pbChoiceDrink14, ui->pbChoiceDrink15,
                                        ui->pbChoiceDrink16};
       for (int i=0; i<16; ++i) ChoiceButtons[i]->setEnabled(false);

       ui->pbCashBack->setEnabled(false);
       ui->pbEnterCash->setEnabled(false);
       timer.start(100);
   }
}


void MainWindow::on_Cooking()
{
    if (ui->progressBar->value() < 100) ui->progressBar->setValue(ui->progressBar->value()+1);
    else
    {
        timer.stop();
        this->finish_cooking();
    }
}


void MainWindow::finish_cooking()
{
    ui->teInformation->setText("Here is your drink");
    ui->teInformation->append("Don't forget delivery");

    QPushButton* ChoiceButtons[16]= {ui->pbChoiceDrink1, ui->pbChoiceDrink2, ui->pbChoiceDrink3,
                                     ui->pbChoiceDrink4, ui->pbChoiceDrink5, ui->pbChoiceDrink6,
                                     ui->pbChoiceDrink7, ui->pbChoiceDrink8, ui->pbChoiceDrink9,
                                     ui->pbChoiceDrink10, ui->pbChoiceDrink11, ui->pbChoiceDrink12,
                                     ui->pbChoiceDrink13, ui->pbChoiceDrink14, ui->pbChoiceDrink15,
                                     ui->pbChoiceDrink16};
    QLineEdit* DrinkNames[16] = {ui->leDrinkName1, ui->leDrinkName2, ui->leDrinkName3, ui->leDrinkName4,
                                ui->leDrinkName5, ui->leDrinkName6, ui->leDrinkName7, ui->leDrinkName8,
                                ui->leDrinkName9, ui->leDrinkName10, ui->leDrinkName11, ui->leDrinkName12,
                                ui->leDrinkName13, ui->leDrinkName14, ui->leDrinkName15, ui->leDrinkName16};
    for (int i=0; i<16; ++i) if (!DrinkNames[i]->text().isEmpty()) ChoiceButtons[i]->setEnabled(true);

    ui->pbCashBack->setEnabled(true);
    ui->pbEnterCash->setEnabled(true);
    ui->leCashBack->setText(QString::number(coffee_machine->finish()));
    ui->progressBar->setValue(0);

    QTime time = QTime::currentTime().addSecs(3);
    while (QTime::currentTime() < time );
    ui->teInformation->setText("Enter the cash");
    ui->leCashBack->setText("");
}


void MainWindow::on_pbCashBack_clicked()
{
    if (coffee_machine->print_state() == "Wait") ui->teInformation->setText("Nothing to return");
    else
    {
        ui->teInformation->setText("Don't forget your money");
        ui->leCashBack->setText(QString::number(coffee_machine->cancel()));
    }
}


void MainWindow::on_pbChoiceDrink1_clicked()
{
    QString drinkname = ui->leDrinkName1->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink2_clicked()
{
    QString drinkname = ui->leDrinkName2->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink3_clicked()
{
    QString drinkname = ui->leDrinkName3->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink4_clicked()
{
    QString drinkname = ui->leDrinkName4->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink5_clicked()
{
    QString drinkname = ui->leDrinkName5->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink6_clicked()
{
    QString drinkname = ui->leDrinkName6->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink7_clicked()
{
    QString drinkname = ui->leDrinkName7->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink8_clicked()
{
    QString drinkname = ui->leDrinkName8->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink9_clicked()
{
    QString drinkname = ui->leDrinkName9->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink10_clicked()
{
    QString drinkname = ui->leDrinkName10->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink11_clicked()
{
    QString drinkname = ui->leDrinkName11->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink12_clicked()
{
    QString drinkname = ui->leDrinkName12->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink13_clicked()
{
    QString drinkname = ui->leDrinkName13->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink14_clicked()
{
    QString drinkname = ui->leDrinkName14->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink15_clicked()
{
    QString drinkname = ui->leDrinkName15->text();
    this->choice_drink(drinkname);
}


void MainWindow::on_pbChoiceDrink16_clicked()
{
    QString drinkname = ui->leDrinkName16->text();
    this->choice_drink(drinkname);
}
