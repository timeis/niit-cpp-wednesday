#include "automata.h"


Automata::Automata(const std::string pathtomenu)
{
    state = OFF;
    cash = 0;

    xml_parser(pathtomenu);
    // parsing from .txt file
//    std::ifstream filemenu;
//    filemenu.open(pathtomenu.c_str());
//    if (!filemenu.is_open()) throw Wrong_Path(); //it is OK? or need something else?
//    std::string name;
//    int price = 0;
//    while (filemenu >> name)
//    {
//        filemenu >> price;
//        menu[name] = price;
//    }
}


void Automata::xml_parser(std::string pathtoxml)
{
    QFile* file = new QFile(pathtoxml.c_str());
    file->open(QIODevice::ReadOnly | QIODevice::Text);
    if (!file->isOpen()) throw Wrong_Path();
    QXmlStreamReader xml(file);
    QXmlStreamReader::TokenType token;
    std::vector<std::string> Names;
    std::vector<int> Prices;

    while (!xml.atEnd() && !xml.hasError())
    {
        token = xml.readNext();
        if (token == QXmlStreamReader::StartDocument) continue;
        if (token == QXmlStreamReader::StartElement)
        {
            if (xml.name() == "drinks") continue;
            if (xml.name() == "drink") continue;
            if (xml.name() == "name")
            {
                xml.readNext();
                Names.push_back(xml.text().toString().toStdString());
                continue;
            }
            if (xml.name() == "price")
            {
                xml.readNext();
                Prices.push_back(xml.text().toInt());
                continue;
            }
        }
    }
    if (Names.size() == Prices.size())
        for (int i=0;i<Names.size();++i)
            menu[Names[i]]=Prices[i];
    else throw Error_In_File();
}


void Automata::on()
{
    if (state == OFF)
    {
        state = WAIT;
        cash = 0;
    }
    else qDebug() << QString::fromStdString("Automata state is already on");
}


void Automata::off()
{
    if (state == WAIT)
    {
        state = OFF;
        cash = 0;
    }
    else qDebug() << QString::fromStdString("Automata state must be WAIT");
}


void Automata::coin(int sum)
{
    if (state == WAIT || state == ACCEPT || state == CHECK)
    {
        state = ACCEPT;
        cash+=sum;
    }
    else qDebug() << QString::fromStdString("Automata state is must WAIT, ACCEPT or CHECK");
}


std::string Automata::print_state() const
{
    switch(state)
    {
    case OFF: return "Off";
    case WAIT: return "Wait";
    case ACCEPT: return "Accept";
    case CHECK: return "Check";
    case COOK: return "Cook";
    }
    return "ERROR";
}


void Automata::choice()
{
    if (state == ACCEPT || state == WAIT)
        state = CHECK;
    else qDebug() << QString::fromStdString("Automata state is must be ACCEPT");
}


int Automata::check(const std::string& drinkname) const
{
    if (state == CHECK)
    return (cash >= menu.at(drinkname))? 0: menu.at(drinkname)-cash;
    else
    {
        qDebug() << QString::fromStdString("Automata state is must be CHECK");
        return -1;
    }
}


void Automata::cook(const std::string& drinkname)
{
    if (state == CHECK)
    {
        state = COOK;
        cash -= menu[drinkname];
        proceed += menu[drinkname];
    }
    else
    {
        qDebug() << QString::fromStdString("Automata state is must be CHECK");
    }
}


int Automata::cancel()
{
    if (state == ACCEPT || state == CHECK)
    {
        state = WAIT;
        int balance = cash;
        cash = 0;
        return balance;
    }
    else
    {
        qDebug() << QString::fromStdString("Automata state is must be CHECK or ACCEPT");
        return -1;
    }
}


int Automata::finish()
{
    if (state == COOK)
    {
        state = WAIT;
        int delivery = cash;
        cash = 0;
        return delivery;
    }
    else
    {
        qDebug() << QString::fromStdString("Automata state is must be COOK");
        return -1;
    }
}
