#ifndef AUTOMATA_H
#define AUTOMATA_H

#include <map>
#include <string>
#include <fstream>
#include <QDebug>
#include <QFile>
#include <QXmlStreamReader>
#include <vector>

struct Wrong_Path {};
struct Error_In_File {};
class Automata: public QObject
{

private:
    enum STATES { OFF ,WAIT ,ACCEPT ,CHECK ,COOK };
    STATES state;
    int cash;
    std::map <std::string, int> menu;
    int proceed;
    void xml_parser(std::string);
public:

    Automata(const std::string);
    void on();
    void off();
    void coin(int);
    const std::map <std::string, int> print_menu() const {return menu;}
    std::string print_state() const;
    void choice();
    int check(const std::string&) const; // if don't enought cash returns difference between the price and the cash, if everything ok - returns 0, oterwise returns -1
    int get_cash() const {return cash;}
    void cook(const std::string&);
    int cancel();// if everyrhing is OK returns cash balance, otherwise returns -1.
    int finish();// it works similarly to cancel(), except state = COOK.
    int get_proceed() const {return proceed;}
    void set_to_zero_proceed() {proceed = 0;}
};

#endif // AUTOMATA_H
