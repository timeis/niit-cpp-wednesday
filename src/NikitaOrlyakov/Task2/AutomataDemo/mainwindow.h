#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPixmap>
#include "automata.h"
#include <QTimer>
#include <QTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_pbEnterCash_clicked();

    void on_pbChoiceDrink1_clicked();

    void on_pbChoiceDrink2_clicked();

    void on_pbChoiceDrink3_clicked();

    void on_pbChoiceDrink4_clicked();

    void on_pbChoiceDrink5_clicked();

    void on_pbChoiceDrink6_clicked();

    void on_pbChoiceDrink7_clicked();

    void on_pbChoiceDrink8_clicked();

    void on_pbChoiceDrink9_clicked();

    void on_pbChoiceDrink10_clicked();

    void on_pbChoiceDrink11_clicked();

    void on_pbChoiceDrink12_clicked();

    void on_pbChoiceDrink13_clicked();

    void on_pbChoiceDrink14_clicked();

    void on_pbChoiceDrink15_clicked();

    void on_pbChoiceDrink16_clicked();

    void on_Cooking();

    void on_pbCashBack_clicked();

private:
    Ui::MainWindow *ui;
    Automata* coffee_machine;
    void choice_drink(const QString&);
    void finish_cooking();
    QTimer timer;
};

#endif // MAINWINDOW_H
