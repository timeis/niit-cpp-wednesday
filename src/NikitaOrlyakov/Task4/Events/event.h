#ifndef EVENT_H
#define EVENT_H
#include <QDateTime>
#include <string>

class Event
{
    QDateTime dtbegin, dtend;
    QString category, evname;

    friend bool compByCategory(const Event*, const Event*);
    friend bool compByBegin(const Event*, const Event*);
    friend bool compByEnd(const Event*, const Event*);
public:
    Event(QString);
    QString getCategory() const;
    QString getEventName() const;
    QString getBeginTime() const;
    QString getEndTime() const;

    int getDurationDays() const;
    int getRemainDays() const;
    int getPastDays() const;
    int getProgress() const;
};


bool compByCategory(const Event* ev1, const Event* ev2);


bool compByBegin(const Event* ev1, const Event* ev2);


bool compByEnd(const Event* ev1, const Event* ev2);

#endif // EVENT_H

