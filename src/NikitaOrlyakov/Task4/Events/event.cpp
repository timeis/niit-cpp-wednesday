#include "event.h"


Event::Event(QString str)
{
    QRegExp re1("::");
    QStringList list = str.split(re1, QString::SkipEmptyParts);
    this->category = list.at(0);
    this->evname = list.at(1);

    QString begin = list.at(2);
    QString end = list.at(3);
    QRegExp re2("-");
    QStringList blist = begin.split(re2, QString::SkipEmptyParts);
    QStringList elist = end.split(re2, QString::SkipEmptyParts);

    this->dtbegin.setDate(QDate(blist.at(2).toInt(), blist.at(1).toInt(), blist.at(0).toInt()));
    this->dtbegin.setTime(QTime(blist.at(3).toInt(), blist.at(4).toInt(), blist.at(5).toInt()));

    this->dtend.setDate(QDate(elist.at(2).toInt(), elist.at(1).toInt(), elist.at(0).toInt()));
    this->dtend.setTime(QTime(elist.at(3).toInt(), elist.at(4).toInt(), elist.at(5).toInt()));
}


QString Event::getCategory() const
{
    return this->category;
}


QString Event::getEventName() const
{
    return this->evname;
}


QString Event::getBeginTime() const
{
    return (QString)this->dtbegin.date().toString();
}


QString Event::getEndTime() const
{
    return (QString)this->dtend.date().toString();
}


int Event::getDurationDays() const
{
    return (int)this->dtbegin.daysTo(this->dtend);
}


int Event::getRemainDays() const
{
    QDateTime today = QDateTime::currentDateTime();
    return (int)today.secsTo(this->dtend)/86400;
}


int Event::getPastDays() const
{
    QDateTime today = QDateTime::currentDateTime();
    return (int)this->dtbegin.daysTo(today);
}


int Event::getProgress() const
{
    QDateTime today = QDateTime::currentDateTime();
    int diff = this->dtbegin.secsTo(today);
    if (diff*100/this->dtbegin.secsTo(this->dtend) < 0) return 0;
    if (diff*100/this->dtbegin.secsTo(this->dtend) > 100) return 100;
    return diff*100/this->dtbegin.secsTo(this->dtend);
}


bool compByCategory(const Event* ev1, const Event* ev2)
{
    return (ev1->category < ev2->category);
}


bool compByBegin(const Event* ev1, const Event* ev2)
{
    return (ev1->dtbegin < ev2->dtbegin);
}


bool compByEnd(const Event* ev1, const Event* ev2)
{
    return (ev1->dtend < ev2->dtend);
}
