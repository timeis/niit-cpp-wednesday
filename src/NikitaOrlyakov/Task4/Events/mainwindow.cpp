#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->timer = new QTimer;
    timer->setInterval(1000);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateData()));
    QStringList Headers;
    Headers << "Category" << "Event" << "Begin" << "End" << "Remain (day)" << "Progress";
    ui->tableWidget->setColumnCount(6);
    ui->tableWidget->setHorizontalHeaderLabels(Headers);
    ui->tableWidget->resizeColumnsToContents();
    ui->pbSortBegin->setEnabled(false);
    ui->pbSortCateg->setEnabled(false);
    ui->pbSortEnd->setEnabled(false);

}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pbLoad_clicked()
{
    try
    {
        std::ifstream fileevent;
        fileevent.open(QFileDialog::getOpenFileName(this,"Events", "/home/" , "txt files (*.txt)").toUtf8());
        if (!fileevent.is_open()) throw WrongPath();
        std::string line;
        while (std::getline(fileevent,line))
        {
            if (!line.empty())
                this->events.push_back(new Event(QString::fromStdString(line)));
        }
        filltable();
        ui->pbSortBegin->setEnabled(true);
        ui->pbSortCateg->setEnabled(true);
        ui->pbSortEnd->setEnabled(true);
        timer->start();
    }
    catch (WrongPath())
    {
        QMessageBox::information(this, "Error", "Wrong path to file");
    }
}


void MainWindow::filltable()
{
    this->progress.clear();

    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(this->events.size());

    QTableWidgetItem *item1, *item2, *item3, *item4, *item5;
    for (int row=0;row != ui->tableWidget->rowCount(); ++row)
    {
        item1 = new QTableWidgetItem(this->events.at(row)->getCategory());
        item1->setTextAlignment(Qt::AlignCenter | Qt::AlignRight);
        ui->tableWidget->setItem(row, 0, item1);

        item2 = new QTableWidgetItem(this->events.at(row)->getEventName());
        item2->setTextAlignment(Qt::AlignCenter | Qt::AlignRight);
        ui->tableWidget->setItem(row, 1, item2);

        item3 = new QTableWidgetItem(this->events.at(row)->getBeginTime());
        item3->setTextAlignment(Qt::AlignCenter | Qt::AlignRight);
        ui->tableWidget->setItem(row, 2, item3);

        item4 = new QTableWidgetItem(this->events.at(row)->getEndTime());
        item4->setTextAlignment(Qt::AlignCenter | Qt::AlignRight);
        ui->tableWidget->setItem(row, 3, item4);

        item5 = new QTableWidgetItem;
        item5->setData(Qt::DisplayRole, this->events.at(row)->getRemainDays());
        item5->setTextAlignment(Qt::AlignCenter | Qt::AlignRight);
        ui->tableWidget->setItem(row, 4, item5);

        QProgressBar* temp = new QProgressBar();
        temp->setValue(this->events.at(row)->getProgress());
        this->progress.push_back(temp);
        ui->tableWidget->setCellWidget(row, 5, temp);
    }

    ui->tableWidget->resizeColumnsToContents();
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);
}


void MainWindow::updateData()
{
    for (int row=0;row != ui->tableWidget->rowCount(); ++row)
    {
        ui->tableWidget->item(row, 4)->setData(Qt::DisplayRole, this->events.at(row)->getRemainDays());
        this->progress.at(row)->setValue(this->events.at(row)->getProgress());
    }
}


void MainWindow::on_pbSortCateg_clicked()
{
    std::sort(this->events.begin(), this->events.end(), compByCategory);
    filltable();
}


void MainWindow::on_pbSortBegin_clicked()
{
    std::sort(this->events.begin(), this->events.end(), compByBegin);
    filltable();
}


void MainWindow::on_pbSortEnd_clicked()
{
    std::sort(this->events.begin(), this->events.end(), compByEnd);
    filltable();
}
