#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QTableWidgetItem>
#include <QFileDialog>
#include <QProgressBar>
#include <QMessageBox>

#include <algorithm>
#include <vector>
#include <fstream>
#include <string>

#include "event.h"

struct WrongPath{};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbLoad_clicked();
    void updateData();

    void on_pbSortCateg_clicked();

    void on_pbSortBegin_clicked();

    void on_pbSortEnd_clicked();

private:
    Ui::MainWindow *ui;
    void filltable();
    QTimer *timer;
    std::vector<Event*> events;
    std::vector<QProgressBar*> progress;
};

#endif // MAINWINDOW_H
