#-------------------------------------------------
#
# Project created by QtCreator 2016-05-16T15:53:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Events
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    event.cpp

HEADERS  += mainwindow.h \
    event.h

FORMS    += mainwindow.ui
