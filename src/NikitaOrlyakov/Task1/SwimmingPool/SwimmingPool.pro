#-------------------------------------------------
#
# Project created by QtCreator 2016-03-31T15:28:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SwimmingPool
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    circle.cpp

HEADERS  += mainwindow.h \
    circle.h

FORMS    += mainwindow.ui
