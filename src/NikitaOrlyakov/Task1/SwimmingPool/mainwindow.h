#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include "circle.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_lePricePaving_editingFinished();

    void on_lePriceFence_editingFinished();

    void on_leWidthLane_editingFinished();

    void on_leRadiusPool_editingFinished();

    void on_pbCalculate_clicked();

private:
    Ui::MainWindow *ui;
    Circle pool, fence;
};

#endif // MAINWINDOW_H
