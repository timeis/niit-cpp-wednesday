#ifndef CIRCLE
#define CIRCLE

#include <math.h>

class Circle
{
    double radius;
    double ference;
    double area;

public:
    Circle();
    void set_radius(double);
    void set_ference(double);
    void set_area(double);
    double get_radius() const;
    double get_ference() const;
    double get_area() const;
};

#endif // CIRCLE

