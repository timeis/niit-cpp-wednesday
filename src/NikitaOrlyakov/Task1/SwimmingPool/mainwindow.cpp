#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    pool.set_radius(0.0);
    fence.set_radius(0.0);
    ui->lePriceFence->setText("0");
    ui->leWidthLane->setText("0");
    ui->leRadiusPool->setText("0");
    ui->lePricePaving->setText("0");
    ui->pbCalculate->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbCalculate_clicked()
{
    QString price_paving = ui->lePricePaving->text(),
            price_fence = ui->lePriceFence->text(),
            width_lane = ui->leWidthLane->text(),
            radius_pool = ui->leRadiusPool->text();
    try
    {
        pool.set_radius(radius_pool.toDouble());
        if (price_paving.toDouble() <0 || price_fence.toDouble()<0 || width_lane.toDouble()<0)
            throw "Negative value";
        fence.set_radius(radius_pool.toDouble()+2*width_lane.toDouble());

        double cost_paving = ((fence.get_area()-pool.get_area())*price_paving.toDouble());
        double cost_fence = fence.get_ference()*price_fence.toDouble();
        ui->leCostPaving->setText(QString::number(cost_paving));
        ui->leCostFence->setText(QString::number(cost_fence));
        ui->leTotalCost->setText(QString::number(cost_paving+cost_fence));
    }
    catch (const char* error)
    {
        QMessageBox::information(this, "Error", error);
    }
}

void MainWindow::on_lePricePaving_editingFinished()
{
    if (ui->lePriceFence->isModified() && ui->leWidthLane->isModified() && ui->leRadiusPool->isModified())
        ui->pbCalculate->setEnabled(true);
}

void MainWindow::on_lePriceFence_editingFinished()
{
    if (ui->lePricePaving->isModified() && ui->leWidthLane->isModified() && ui->leRadiusPool->isModified())
        ui->pbCalculate->setEnabled(true);
}

void MainWindow::on_leWidthLane_editingFinished()
{
    if (ui->lePriceFence->isModified() && ui->lePricePaving->isModified() && ui->leRadiusPool->isModified())
        ui->pbCalculate->setEnabled(true);
}

void MainWindow::on_leRadiusPool_editingFinished()
{
    if (ui->lePriceFence->isModified() && ui->leWidthLane->isModified() && ui->lePricePaving->isModified())
        ui->pbCalculate->setEnabled(true);
}


