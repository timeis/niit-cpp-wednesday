#include "circle.h"

Circle::Circle() {radius=0;ference=0;area=0;}

void Circle::set_radius(double r)
{
    if (r < 0) throw "Negative value";
    radius = r;
    ference=2*M_PI*r;
    area=M_PI*r*r;
}

void Circle::set_ference(double f)
{
    if (f < 0) throw "Negative value";
    ference = f;
    radius=f/(2*M_PI);
    area=f*f/(4*M_PI);
}

void Circle::set_area(double a)
{
    if (a < 0) throw "Negative value";
    area = a;
    radius=sqrt(a/M_PI);
    ference=sqrt(a*4*M_PI);
}

double Circle::get_radius() const {return radius;}
double Circle::get_ference() const{return ference;}
double Circle::get_area() const {return area;}
