#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _circle.set_radius(0.0);
    ui->leRadius->setText("0");
    ui->leFerence->setText("0");
    ui->leArea->setText("0");
    ui->pbRadius->setEnabled(false);
    ui->pbArea->setEnabled(false);
    ui->pbFerence->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbRadius_clicked()
{
    QString r=ui->leRadius->text();
    try
    {
        _circle.set_radius(r.toDouble());
        ui->leFerence->setText(QString::number(_circle.get_ference()));
        ui->leArea->setText(QString::number(_circle.get_area()));
    }
    catch(const char* error)
    {
        QMessageBox::information(this,"Error",error);
    }
    ui->pbRadius->setEnabled(false);
    ui->pbArea->setEnabled(false);
    ui->pbFerence->setEnabled(false);
}

void MainWindow::on_pbFerence_clicked()
{
    QString f=ui->leFerence->text();
    try
    {
    _circle.set_ference(f.toDouble());
    ui->leRadius->setText(QString::number(_circle.get_radius()));
    ui->leArea->setText(QString::number(_circle.get_area()));
    }
    catch(const char* error)
    {
        QMessageBox::information(this,"Error",error);
    }
    ui->pbRadius->setEnabled(false);
    ui->pbArea->setEnabled(false);
    ui->pbFerence->setEnabled(false);
}

void MainWindow::on_pbArea_clicked()
{
    QString a=ui->leArea->text();
    try
    {
    _circle.set_area(a.toDouble());
    ui->leFerence->setText(QString::number(_circle.get_ference()));
    ui->leRadius->setText(QString::number(_circle.get_radius()));
    }
    catch(const char* error)
    {
        QMessageBox::information(this,"Error",error);
    }
    ui->pbRadius->setEnabled(false);
    ui->pbArea->setEnabled(false);
    ui->pbFerence->setEnabled(false);
}



void MainWindow::on_leRadius_editingFinished()
{
    ui->pbRadius->setEnabled(true);
}

void MainWindow::on_leFerence_editingFinished()
{
    ui->pbFerence->setEnabled(true);
}

void MainWindow::on_leArea_editingFinished()
{
    ui->pbArea->setEnabled(true);
}
