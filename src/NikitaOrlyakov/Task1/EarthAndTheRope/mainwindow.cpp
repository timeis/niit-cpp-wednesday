#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    rope.set_radius(0.0);
    ui->leEarthRadius->setText("0");
    ui->lePieceOfRope->setText("0");
    ui->pbCalculate->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbCalculate_clicked()
{
    QString earth_radius = ui->leEarthRadius->text(),
            piece_rope = ui->lePieceOfRope->text();
    try
    {
        rope.set_radius(earth_radius.toDouble());
        if (piece_rope.toDouble() < 0) throw "Negative value";
        rope.set_ference(rope.get_ference()+piece_rope.toDouble());
        ui->leGap->setText(QString::number(rope.get_radius()-earth_radius.toDouble()));
    }
    catch (const char* error)
    {
        QMessageBox::information(this, "Error", error);
    }
    ui->pbCalculate->setEnabled(false);
}

void MainWindow::on_leEarthRadius_editingFinished()
{
    if (ui->lePieceOfRope->isModified()) ui->pbCalculate->setEnabled(true);
}

void MainWindow::on_lePieceOfRope_editingFinished()
{
    if (ui->lePieceOfRope->isModified()) ui->pbCalculate->setEnabled(true);
}
