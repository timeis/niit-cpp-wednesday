#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "circle.h"
#include <QMessageBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbCalculate_clicked();

    void on_leEarthRadius_editingFinished();

    void on_lePieceOfRope_editingFinished();

private:
    Ui::MainWindow *ui;
    Circle rope;
};

#endif // MAINWINDOW_H
