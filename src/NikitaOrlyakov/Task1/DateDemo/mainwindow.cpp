#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->leYear1->setText("0");
    ui->leMonth1->setText("0");
    ui->leDay1->setText("0");
    ui->leHour1->setText("0");
    ui->leMinute1->setText("0");
    ui->leSecond1->setText("0");
    ui->leMoveDay1->setText("0");
    ui->leYear2->setText("0");
    ui->leMonth2->setText("0");
    ui->leDay2->setText("0");
    ui->leHour2->setText("0");
    ui->leMinute2->setText("0");
    ui->leSecond2->setText("0");
    ui->leMoveDay2->setText("0");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbToday1_clicked()
{
   date1.set_current_date();
   ui->leYear1->setText(QString::number(date1.get_today().tm_year+1900));
   ui->leMonth1->setText(QString::number(date1.get_today().tm_mon+1));
   ui->leDay1->setText(QString::number(date1.get_today().tm_mday));
   ui->leHour1->setText(QString::number(date1.get_today().tm_hour));
   ui->leMinute1->setText(QString::number(date1.get_today().tm_min));
   ui->leSecond1->setText(QString::number(date1.get_today().tm_sec));
}

void MainWindow::on_pbSet1_clicked()
{
    QString year = ui->leYear1->text(),
            month = ui->leMonth1->text(),
            day = ui->leDay1->text(),
            hour = ui->leHour1->text(),
            minute = ui->leMinute1->text(),
            second = ui->leSecond1->text();
    try
    {
    date1.set_date(year.toInt(),month.toInt(),day.toInt(),hour.toInt(),minute.toInt(),second.toInt());
    }
    catch (const char* error)
    {
        QMessageBox::information(this, "Error", error);
    }
}

void MainWindow::on_pbToday2_clicked()
{
    date2.set_current_date();
    ui->leYear2->setText(QString::number(date2.get_today().tm_year+1900));
    ui->leMonth2->setText(QString::number(date2.get_today().tm_mon+1));
    ui->leDay2->setText(QString::number(date2.get_today().tm_mday));
    ui->leHour2->setText(QString::number(date2.get_today().tm_hour));
    ui->leMinute2->setText(QString::number(date2.get_today().tm_min));
    ui->leSecond2->setText(QString::number(date2.get_today().tm_sec));
}

void MainWindow::on_pbSet2_clicked()
{
    QString year = ui->leYear2->text(),
            month = ui->leMonth2->text(),
            day = ui->leDay2->text(),
            hour = ui->leHour2->text(),
            minute = ui->leMinute2->text(),
            second = ui->leSecond2->text();

    try
    {
    date2.set_date(year.toInt(),month.toInt(),day.toInt(),hour.toInt(),minute.toInt(),second.toInt());
    }
    catch (const char* error)
    {
        QMessageBox::information(this, "Error", error);
    }
}

void MainWindow::on_pbMoveLeft1_clicked()  // !!DOESNT CHANGE date1; only print the date before date1 on N days!!
{
    QString moveday = ui->leMoveDay1->text();
    try
    {
    ui->leYear1->setText(QString::number(date1.get_past(moveday.toInt()).tm_year+1900));
    ui->leMonth1->setText(QString::number(date1.get_past(moveday.toInt()).tm_mon+1));
    ui->leDay1->setText(QString::number(date1.get_past(moveday.toInt()).tm_mday));
    ui->leHour1->setText(QString::number(date1.get_past(moveday.toInt()).tm_hour));
    ui->leMinute1->setText(QString::number(date1.get_past(moveday.toInt()).tm_min));
    ui->leSecond1->setText(QString::number(date1.get_past(moveday.toInt()).tm_sec));
    }
    catch (const char* error)
    {
        QMessageBox::information(this, "Error", error);
    }
}

void MainWindow::on_pbMoveRight1_clicked()  // !!DOESNT CHANGE date1; only print the date after date1 on N days!!
{
    QString moveday = ui->leMoveDay1->text();
    try
    {
    ui->leYear1->setText(QString::number(date1.get_future(moveday.toInt()).tm_year+1900));
    ui->leMonth1->setText(QString::number(date1.get_future(moveday.toInt()).tm_mon+1));
    ui->leDay1->setText(QString::number(date1.get_future(moveday.toInt()).tm_mday));
    ui->leHour1->setText(QString::number(date1.get_future(moveday.toInt()).tm_hour));
    ui->leMinute1->setText(QString::number(date1.get_future(moveday.toInt()).tm_min));
    ui->leSecond1->setText(QString::number(date1.get_future(moveday.toInt()).tm_sec));
    }
    catch (const char* error)
    {
        QMessageBox::information(this, "Error", error);
    }
}

void MainWindow::on_pbMoveLeft2_clicked() // !!DOESNT CHANGE date2; only print the date before date2 on N days!!
{
    QString moveday = ui->leMoveDay2->text();
    try
    {
    ui->leYear2->setText(QString::number(date2.get_past(moveday.toInt()).tm_year+1900));
    ui->leMonth2->setText(QString::number(date2.get_past(moveday.toInt()).tm_mon+1));
    ui->leDay2->setText(QString::number(date2.get_past(moveday.toInt()).tm_mday));
    ui->leHour2->setText(QString::number(date2.get_past(moveday.toInt()).tm_hour));
    ui->leMinute2->setText(QString::number(date2.get_past(moveday.toInt()).tm_min));
    ui->leSecond2->setText(QString::number(date2.get_past(moveday.toInt()).tm_sec));
    }
    catch (const char* error)
    {
        QMessageBox::information(this, "Error", error);
    }
}

void MainWindow::on_pbMoveRight2_clicked() // !!DOESNT CHANGE date2; only print the date after date2 on N days!!
{
    QString moveday = ui->leMoveDay2->text();
    try
    {
    ui->leYear2->setText(QString::number(date2.get_future(moveday.toInt()).tm_year+1900));
    ui->leMonth2->setText(QString::number(date2.get_future(moveday.toInt()).tm_mon+1));
    ui->leDay2->setText(QString::number(date2.get_future(moveday.toInt()).tm_mday));
    ui->leHour2->setText(QString::number(date2.get_future(moveday.toInt()).tm_hour));
    ui->leMinute2->setText(QString::number(date2.get_future(moveday.toInt()).tm_min));
    ui->leSecond2->setText(QString::number(date2.get_future(moveday.toInt()).tm_sec));
    }
    catch (const char* error)
    {
        QMessageBox::information(this, "Error", error);
    }
}

void MainWindow::on_pbCalcDiff_clicked()
{
    ui->leDiffSec->setText(QString::number(date1.calc_difference_sec(date2)));
    ui->leDiffDay->setText(QString::number(date1.calc_difference_day(date2)));

    // if user forgets to press set (for left or right date), he will see for which date values was calculated:

    ui->leYear1->setText(QString::number(date1.get_today().tm_year+1900));
    ui->leMonth1->setText(QString::number(date1.get_today().tm_mon+1));
    ui->leDay1->setText(QString::number(date1.get_today().tm_mday));
    ui->leHour1->setText(QString::number(date1.get_today().tm_hour));
    ui->leMinute1->setText(QString::number(date1.get_today().tm_min));
    ui->leSecond1->setText(QString::number(date1.get_today().tm_sec));

    ui->leYear2->setText(QString::number(date2.get_today().tm_year+1900));
    ui->leMonth2->setText(QString::number(date2.get_today().tm_mon+1));
    ui->leDay2->setText(QString::number(date2.get_today().tm_mday));
    ui->leHour2->setText(QString::number(date2.get_today().tm_hour));
    ui->leMinute2->setText(QString::number(date2.get_today().tm_min));
    ui->leSecond2->setText(QString::number(date2.get_today().tm_sec));
}
