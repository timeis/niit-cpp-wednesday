#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "datedemo.h"
#include <QMessageBox>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbToday1_clicked();

    void on_pbSet1_clicked();

    void on_pbToday2_clicked();

    void on_pbSet2_clicked();

    void on_pbMoveLeft1_clicked();

    void on_pbMoveRight1_clicked();

    void on_pbMoveLeft2_clicked();

    void on_pbMoveRight2_clicked();

    void on_pbCalcDiff_clicked();

private:
    Ui::MainWindow *ui;
    DateDemo date1, date2;

};

#endif // MAINWINDOW_H
