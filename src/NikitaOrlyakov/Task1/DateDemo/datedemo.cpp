#include "datedemo.h"

std::string NameMonth[12] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
std::string NameWDay[7] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

DateDemo::DateDemo():now(time(0)) {}

DateDemo::DateDemo(int year, int month, int day, int hour, int min, int sec)
{
    if (month<=0 || day<=0 || hour <0 || min<0 || sec<0) throw "Invalid value";
    tm *date = new tm;
    date->tm_year = (year-1900);
    date->tm_mon = month-1;
    date->tm_mday = day;
    date->tm_hour = hour;
    date->tm_min = min;
    date->tm_sec = sec;

    now = mktime(date);

    delete date;
    date = 0;
}

DateDemo::DateDemo(const DateDemo& t):now(t.now){}

DateDemo& DateDemo::operator= (const DateDemo& t)
{\
    now = t.now;
    return *this;
}

void DateDemo::set_date(int year, int month, int day, int hour, int min, int sec)
{
    if (month<=0 || day<=0 || hour <0 || min<0 || sec<0) throw "Invalid value";
    tm *date = new tm;
    date->tm_year = (year-1900);
    date->tm_mon = month-1;
    date->tm_mday = day;
    date->tm_hour = hour;
    date->tm_min = min;
    date->tm_sec = sec;

    now = mktime(date);

    delete date;
    date = 0;
}

void DateDemo::set_current_date()
{
    time(&now);
}

tm DateDemo::get_today() const
{
    tm date = *localtime(&now);
    return date;
}

tm DateDemo::get_yesterday() const
{
    time_t yest = now - 24*60*60;
    tm date = *localtime(&yest);
    return date;
}

tm DateDemo::get_tommorow() const
{
    time_t tomm = now + 24*60*60;
    tm date = *localtime(&tomm);
    return date;
}

tm DateDemo::get_past(int day)const
{
    if (day<0) throw "Negative value";
    time_t past = now - day*24*60*60;
    tm date = *localtime(&past);
    return date;
}

tm DateDemo::get_future(int day) const
{
    if (day<0) throw "Negative value";
    time_t future = now + day*24*60*60;
    tm date = *localtime(&future);
    return date;
}

std::string DateDemo::print_today() const
{
    tm *date = localtime(&now);
    std::string strdate = asctime(date);
    return strdate;
}

std::string DateDemo::print_yesterday() const
{
    time_t yest = now - 24*60*60;
    tm *date = localtime(&yest);
    std::string strdate = asctime(date);
    return strdate;
}

std::string DateDemo::print_tommorow() const
{
    time_t tomm = now + 24*60*60;
    tm *date = localtime(&tomm);
    std::string strdate = asctime(date);
    return strdate;
}

std::string DateDemo::print_month() const
{
    tm *date = localtime(&now);
    std::string strmonth = NameMonth[date->tm_mon];
    return strmonth;
}

std::string DateDemo::print_wday() const
{
    tm *date = localtime(&now);
    std::string strmonth = NameWDay[date->tm_wday];
    return strmonth;
}

int DateDemo::calc_difference_day(const DateDemo& t)
{
    return (this->now > t.now) ? (this->now - t.now)/86400 : (t.now - this->now)/86400;
}

int DateDemo::calc_difference_sec(const DateDemo& t)
{
    return (this->now > t.now) ? (this->now - t.now) : (t.now - this->now);
}
