#-------------------------------------------------
#
# Project created by QtCreator 2016-03-30T20:05:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DateDemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    datedemo.cpp

HEADERS  += mainwindow.h \
    datedemo.h

FORMS    += mainwindow.ui
