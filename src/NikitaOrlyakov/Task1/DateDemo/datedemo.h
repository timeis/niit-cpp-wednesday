#ifndef DATEDEMO_H
#define DATEDEMO_H
#include <time.h>
#include <string>

class DateDemo
{
private:
    time_t now;

public:
    DateDemo();
    DateDemo(int, int, int, int =0, int =0, int =0);
    DateDemo(const DateDemo&);
    DateDemo& operator= (const DateDemo&);
    void set_date(int, int, int, int =0, int =0, int =0);
    void set_current_date();
    tm get_today() const;
    tm get_yesterday() const;
    tm get_tommorow() const;
    tm get_past(int)const;
    tm get_future(int) const;
    std::string print_today() const;
    std::string print_yesterday() const;
    std::string print_tommorow() const;
    std::string print_month() const;
    std::string print_wday() const;
    int calc_difference_day(const DateDemo&);
    int calc_difference_sec(const DateDemo&);
    std::string NameMonth[12];
    std::string NameWDay[7];
};

#endif // DATEDEMO_H
