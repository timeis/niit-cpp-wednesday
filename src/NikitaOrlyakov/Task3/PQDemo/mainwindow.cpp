#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTableView>
#include <QStandardItemModel>
#include <QStringList>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    PQ = new PQueue();
    filltable();
    ui->leLength->setText(QString::number(PQ->PQlength()));
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::filltable()
{
    QStandardItemModel *model = new QStandardItemModel(0,2);
    QStringList Headers;
    Headers << "Priority" << "Note";
    model->setHorizontalHeaderLabels(Headers);
    ui->tableView->setModel(model);
    model->setRowCount(PQ->PQreturn()->size());

    QStandardItem *item1, *item2;
    std::multimap<int,std::string>::const_reverse_iterator iter = PQ->PQreturn()->rbegin();
    for (int row=0;row !=model->rowCount(); ++row)
    {
        item1 = new QStandardItem(QString::number(iter->first));
        item1->setTextAlignment(Qt::AlignCenter);
        item2 = new QStandardItem(QString::fromStdString(iter->second));
        item2->setTextAlignment(Qt::AlignCenter | Qt::AlignRight);
        ++iter;
        model->setItem(row,0,item1);
        model->setItem(row,1,item2);
    }
}


void MainWindow::on_pbInsert_clicked()
{
    if (ui->lePriority->text().isEmpty() || ui->leNote->text().isEmpty())
        QMessageBox::information(this, "Error", "Note or Priority is empty");
    else
    {
        PQ->PQinsert(ui->lePriority->text().toInt(),ui->leNote->text().toStdString());
        filltable();
        ui->leLength->setText(QString::number(PQ->PQlength()));
    }
}


void MainWindow::on_pbDelete_clicked()
{
    try
    {
        PQ->PQdelete();
        filltable();
        ui->leLength->setText(QString::number(PQ->PQlength()));
    }
    catch (QueueEmpty)
    {
        QMessageBox::information(this, "Error", "PQueue is empty");
    }
}


void MainWindow::on_pbClear_clicked()
{
    PQ->PQclear();
    filltable();
    ui->leLength->setText(QString::number(PQ->PQlength()));
}
