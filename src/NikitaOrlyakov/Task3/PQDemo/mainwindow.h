#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "pqueue.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbInsert_clicked();

    void on_pbDelete_clicked();

    void on_pbClear_clicked();

private:
    void filltable();
    Ui::MainWindow *ui;
    PQueue* PQ;
};

#endif // MAINWINDOW_H
