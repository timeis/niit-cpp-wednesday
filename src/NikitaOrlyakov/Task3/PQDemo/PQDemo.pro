#-------------------------------------------------
#
# Project created by QtCreator 2016-05-12T12:46:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PQDemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    pqueue.cpp

HEADERS  += mainwindow.h \
    pqueue.h

FORMS    += mainwindow.ui
