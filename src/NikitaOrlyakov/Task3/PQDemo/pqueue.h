#ifndef PQUEUE_H
#define PQUEUE_H
#include <map>
#include <string>

struct QueueEmpty{};

class PQueue
{
    std::multimap<int,std::string>* queue;
public:
    PQueue();
    PQueue(const PQueue&);
    int PQlength() const;
    void PQinsert(const int&,const std::string&);
    std::string PQdelete();
    void PQclear();
    const std::multimap<int,std::string>* PQreturn() const {return this->queue;}
};

#endif // PQUEUE_H
