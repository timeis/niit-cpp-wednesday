#include "pqueue.h"

PQueue::PQueue()
{
    queue = new std::multimap<int,std::string>;
}


PQueue::PQueue(const PQueue& pq1)
{
    queue = new std::multimap<int,std::string>;
    *this->queue = *pq1.queue;
}


int PQueue::PQlength() const
{
    return this->queue->size();
}


void PQueue::PQinsert(const int& key,const std::string& note)
{
    this->queue->insert(make_pair(key,note));
}


std::string PQueue::PQdelete()
{
    if (this->queue->empty()) throw QueueEmpty();
    std::string str = this->queue->rbegin()->second;
    this->queue->erase(--this->queue->end());
    return str;
}


void PQueue::PQclear()
{
    this->queue->clear();
}

