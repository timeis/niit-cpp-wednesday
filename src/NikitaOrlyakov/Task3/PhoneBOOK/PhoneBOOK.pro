#-------------------------------------------------
#
# Project created by QtCreator 2016-05-13T11:12:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PhoneBOOK
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    phonebook.cpp

HEADERS  += mainwindow.h \
    phonebook.h

FORMS    += mainwindow.ui
