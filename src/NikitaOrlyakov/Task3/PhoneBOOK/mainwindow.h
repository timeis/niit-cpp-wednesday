#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItem>
#include <QFileDialog>
#include <QMessageBox>
#include "phonebook.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbLoad_clicked();

    void on_pbShowAll_clicked();

    void on_pbInsert_clicked();

    void on_pbSearchNum_clicked();

    void on_pbSearchName_clicked();

    void on_pbSave_clicked();

private:
    Ui::MainWindow *ui;
    void filltable(const std::multimap<std::string,int>&);
    QStandardItemModel* model;
    PhoneBook* Phones;
};

#endif // MAINWINDOW_H
