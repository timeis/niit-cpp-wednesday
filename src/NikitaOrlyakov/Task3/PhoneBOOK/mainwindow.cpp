#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    model = new QStandardItemModel(0,2);
    QStringList Headers;
    Headers << "Name" << "Number";
    model->setHorizontalHeaderLabels(Headers);
    ui->tableView->setModel(model);
    ui->pbInsert->setEnabled(false);
    ui->pbSave->setEnabled(false);
    ui->pbShowAll->setEnabled(false);
    ui->pbSearchNum->setEnabled(false);
    ui->pbSearchName->setEnabled(false);
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::filltable(const std::multimap<std::string,int>& book)
{
    model->setRowCount(book.size());

    QStandardItem *item1, *item2;
    std::multimap<std::string,int>::const_iterator iter = book.begin();
    for (int row=0;row != model->rowCount(); ++row)
    {
        item1 = new QStandardItem(QString::fromStdString(iter->first));
        item1->setTextAlignment(Qt::AlignCenter);
        item2 = new QStandardItem(QString::number(iter->second));
        item2->setTextAlignment(Qt::AlignCenter | Qt::AlignRight);
        ++iter;
        model->setItem(row,0,item1);
        model->setItem(row,1,item2);
    }
}


void MainWindow::on_pbLoad_clicked()
{
    QString filename;
    filename = QFileDialog::getOpenFileName(this,"Phone book", "/home/" , "txt files (*.txt)");
    try
    {
        Phones = new PhoneBook(filename.toStdString());
        QMessageBox::information(this, "Message", "Successfully loaded");
        ui->pbInsert->setEnabled(true);
        ui->pbSave->setEnabled(true);
        ui->pbShowAll->setEnabled(true);
        ui->pbSearchNum->setEnabled(true);
        ui->pbSearchName->setEnabled(true);
    }
    catch (WrongPath)
    {
        QMessageBox::information(this, "Error", "Wrong path to file");
    }

}


void MainWindow::on_pbShowAll_clicked()
{
    filltable(Phones->PBreturn());
}


void MainWindow::on_pbInsert_clicked()
{
    if (ui->leName->text().isEmpty() || ui->leNumber->text().isEmpty())
        QMessageBox::information(this, "Error", "Name or Number is empty");
    else
        Phones->PBinsert(ui->leName->text().toStdString(), ui->leNumber->text().toInt());
}


void MainWindow::on_pbSearchNum_clicked()
{
    if (ui->leSearch->text().isEmpty())
        QMessageBox::information(this, "Error", "Search line is empty");
    else
        filltable(Phones->PBsearchnumber(ui->leSearch->text().toInt()));
}


void MainWindow::on_pbSearchName_clicked()
{
    if (ui->leSearch->text().isEmpty())
        QMessageBox::information(this, "Error", "Search line is empty");
    else
        filltable(Phones->PBsearchname(ui->leSearch->text().toStdString()));
}


void MainWindow::on_pbSave_clicked()
{
    Phones->PBsaveinfile();
}
