#include "phonebook.h"

PhoneBook::PhoneBook(const std::string& pathtobook)
{
    this->pbook = std::multimap<std::string,int>();
    this->path = pathtobook;

    std::ifstream filebook;
    filebook.open(this->path.c_str());
    if (!filebook.is_open()) throw WrongPath();
    int number = 0;
    while (filebook >> number)
    {
        std::string name;
        filebook >> name;
        char temp = filebook.get();
        while(temp != '\n')
        {
            name += temp;
            temp = filebook.get();
        }
        pbook.insert(make_pair(name, number));
    }
}


void PhoneBook::PBinsert(const std::string& name, const int& number)
{
    this->pbook.insert(make_pair(name, number));
}


std::multimap<std::string,int> PhoneBook::PBsearchnumber(const int& number) const
{
    std::multimap<std::string,int> result;
    for (auto iter: pbook)
        if (iter.second == number) result.insert(iter);
    return result;
}


std::multimap<std::string,int> PhoneBook::PBsearchname(const std::string& name) const
{
    std::multimap<std::string,int> result;
    for (auto iter: pbook)
        if (compare(name, iter.first) == 0) result.insert(iter);
    return result;
}


int PhoneBook::compare(const std::string& str1, const std::string& str2) const
{
    if (str1.size() > str2.size()) return -1;
    for (int i=0;i<(int)str1.size();++i) if (str1[i] != str2[i]) return 1;
    return 0;
}


int PhoneBook::PBsaveinfile() //return 0 if everythink is ok
{
    std::ofstream filebook;
    filebook.open(this->path.c_str());
    for (auto iter: pbook)
        filebook << iter.second << " " << iter.first << std::endl;
    return 0;
}

