#ifndef PHONEBOOK_H
#define PHONEBOOK_H

#include <map>
#include <string>
#include <fstream>

struct WrongPath{};

class PhoneBook
{
    std::string path;
    std::multimap<std::string,int> pbook;
public:
    PhoneBook(const std::string&);
    void PBinsert(const std::string&, const int&);
    std::multimap<std::string,int> PBsearchnumber(const int&) const;
    std::multimap<std::string,int> PBsearchname(const std::string&) const;
    int PBsaveinfile(); //return 0 if everythink is ok
    const std::multimap<std::string,int>& PBreturn() const {return this->pbook;}
    int compare(const std::string&, const std::string&) const; /* return 0 if first string == the begin
    of second string for example: compare("fav","favourite") == 0, but compare("favourite","fav") !=0.*/
};

#endif // PHONEBOOK_H
