#include "longinteger.h"


LongInteger::LongInteger() // default constructor return value = 0;
{
    this->value = std::deque<int>();
    this->value.push_back(0);
    this->sign = ' ';
}


LongInteger::LongInteger(const std::string& str)
{
    this->value = std::deque<int>();
    this->sign = ' ';

    if (str.at(0) == '-') this->sign = '-';
    for (int i = 0; i < (int)str.size(); i++)
        switch (str.at(i))
        {
        case '0': {this->value.push_back(0); break; }
        case '1': {this->value.push_back(1); break; }
        case '2': {this->value.push_back(2); break; }
        case '3': {this->value.push_back(3); break; }
        case '4': {this->value.push_back(4); break; }
        case '5': {this->value.push_back(5); break; }
        case '6': {this->value.push_back(6); break; }
        case '7': {this->value.push_back(7); break; }
        case '8': {this->value.push_back(8); break; }
        case '9': {this->value.push_back(9); break; }
        case '-': {if (i != 0) throw InvalidValue(); break; }
        default: {throw InvalidValue(); }
        }
    this->delete_zero();
}


void LongInteger::delete_zero() // delete zeros from the front of value and if answer is "-0" delete "-"
{
    while (this->value.size() > 1)
    {
        if (this->value.at(0) != 0) break;
        else this->value.pop_front();
    }
    if (this->value == LongInteger().value) this->sign = ' ';
}


std::string LongInteger::toString() const
{
    std::string str;
    if (this->sign == '-') str = "-";
    for (unsigned int i = 0; i < this->value.size(); ++i)
        switch (this->value.at(i))
        {
        case 0: {str += "0"; break; }
        case 1: {str += "1"; break; }
        case 2: {str += "2"; break; }
        case 3: {str += "3"; break; }
        case 4: {str += "4"; break; }
        case 5: {str += "5"; break; }
        case 6: {str += "6"; break; }
        case 7: {str += "7"; break; }
        case 8: {str += "8"; break; }
        case 9: {str += "9"; break; }
        }
    return str;
}


int LongInteger::compare_by_module(const LongInteger& long1) const
{
    if (this->value.size() > long1.value.size()) return 1;
    if (this->value.size() < long1.value.size()) return (-1);
    for (int i=0; i < (int)this->value.size(); ++i)
    {
        if (this->value.at(i) > long1.value.at(i)) return 1;
        if (this->value.at(i) < long1.value.at(i)) return -1;
    }
    return 0;
}


LongInteger LongInteger::operator+(const LongInteger& long1) const
{
    LongInteger sum = LongInteger("-");
    sum.sign = ' ';
    int temp = 0, digit = 0;
    if(this->sign == long1.sign)
    {
        sum.sign = this->sign;
        int i = this->value.size() - 1;
        int j = long1.value.size() - 1;
        while (i >= 0 || j >= 0)
        {
            if (i >= 0 && j >= 0 ) digit = this->value.at(i)+long1.value.at(j)+temp;
            if (j < 0) digit = this->value.at(i)+temp;
            if (i < 0) digit = long1.value.at(j)+temp;
            if (digit >= 10) temp = 1; else temp = 0;
            sum.value.push_front(digit % 10);
            --i; --j;
        }
        if(temp != 0) sum.value.push_front(temp);
        sum.delete_zero();
        return sum;
    }
    else
    {
        if (compare_by_module(long1) == 0) {sum.value.push_front(0); sum.delete_zero(); return sum;}
        if (compare_by_module(long1) == 1)
        {
            sum.sign = this->sign;
            int i = this->value.size() - 1;
            int j = long1.value.size() - 1;
            while (i >= 0)
            {
                if (j >= 0 ) digit = this->value.at(i)-long1.value.at(j)-temp;
                if (j < 0) digit = this->value.at(i)-temp;
                if (digit < 0) { temp = 1; digit +=10;}
                else temp = 0;
                sum.value.push_front(digit);
                --i; --j;
            }
            sum.delete_zero();
            return sum;
        }
        if (compare_by_module(long1) == -1)
        {
            sum.sign = long1.sign;
            int i = this->value.size() - 1;
            int j = long1.value.size() - 1;
            while (j >= 0)
            {
                if (i >= 0) digit = long1.value.at(j)-this->value.at(i)-temp;
                if (i < 0) digit = long1.value.at(j)-temp;
                if (digit < 0) { temp = 1; digit +=10;}
                else temp = 0;
                sum.value.push_front(digit);
                --i; --j;
            }
            sum.delete_zero();
            return sum;
        }
    }
    sum.value.push_back(0); // if something going wrong, function return "0-0"
    sum.value.push_back(-0);
    return sum;
}


LongInteger LongInteger::operator-(LongInteger long1) const
{
    if (long1.sign == '-') long1.sign = ' '; else long1.sign = '-';
    return (*this+long1);
}


LongInteger LongInteger::operator*(const LongInteger& long1) const
{
    LongInteger mult = LongInteger();
    for (int j = long1.value.size() - 1; j >= 0 ; --j)
    {
        LongInteger templong = LongInteger();
        for (int i = 0; i < long1.value.at(j); ++i)
            templong = *this + templong;
        for (int i = 0; i < (int)long1.value.size() - 1 - j; ++i)
            templong.value.push_back(0);
        mult = templong + mult;
    }
    if (this->sign != long1.sign) mult.sign = '-';
    else mult.sign = ' ';
    mult.delete_zero();
    return mult;
}


LongInteger LongInteger::operator/(const LongInteger& long1) const
{
    if (long1.value == LongInteger().value) throw DivideByZero();
    if (compare_by_module(long1) == -1) return LongInteger();

    LongInteger div = LongInteger();
    LongInteger temp = LongInteger();
    temp.value = this->value;
    temp.sign = long1.sign;
    while (temp.compare_by_module(long1) >= 0)
    {
        temp = temp - long1;
        div = LongInteger("1")+div;
    }
    if (this->sign != long1.sign) div.sign = '-';
    div.delete_zero();
    return div;
}


LongInteger LongInteger::operator%(const LongInteger& long1) const
{
    if (long1.value == LongInteger().value) throw DivideByZero();

    LongInteger modulo = LongInteger();
    modulo.value = this->value;
    modulo.sign = long1.sign;
    while (modulo.compare_by_module(long1) >= 0)
        modulo = modulo - long1;
    if (this->sign != long1.sign) modulo.sign = '-';
    modulo.delete_zero();
    return modulo;
}


LongInteger LongInteger::operator^(const LongInteger& long1) const
{
    if (long1.value == LongInteger().value) return LongInteger("1");
    if (long1 == LongInteger("1")) return *this;
    if (long1.sign == '-') throw NegativeInvolution();

    LongInteger invol = LongInteger("1");
    for (LongInteger i = LongInteger(); i < long1; i = i + LongInteger("1"))
        invol = *this * invol;
    invol.delete_zero();
    return invol;
}


bool LongInteger::operator<(const LongInteger& long1) const
{
    if (this->value == LongInteger().value && long1.value == LongInteger().value) return false; //if both of numbers is "0" or "-0"
    if (this->sign == '-' && long1.sign != '-') return true;
    if (this->sign != '-' && long1.sign == '-') return false;
    if (this->sign == '-' && this->value.size() > long1.value.size()) return true;
    if (this->sign == '-' && this->value.size() < long1.value.size()) return false;
    if (this->sign != '-' && this->value.size() > long1.value.size()) return false;
    if (this->sign != '-' && this->value.size() < long1.value.size()) return true;
    if (this->sign != '-')
        for (int i=0; i < (int)this->value.size(); ++i)
        {
            if (this->value.at(i) > long1.value.at(i)) return false;
            if (this->value.at(i) < long1.value.at(i)) return true;
        }
    else
        for (int i=0; i < (int)this->value.size(); ++i)
        {
            if (this->value.at(i) < long1.value.at(i)) return false;
            if (this->value.at(i) > long1.value.at(i)) return true;
        }
    return false;
}


bool LongInteger::operator>(const LongInteger& long1) const
{
    if (this->value == LongInteger().value && long1.value == LongInteger().value) return false; //if both of numbers is "0" or "-0"
    if (this->sign == '-' && long1.sign != '-') return false;
    if (this->sign != '-' && long1.sign == '-') return true;
    if (this->sign == '-' && this->value.size() > long1.value.size()) return false;
    if (this->sign == '-' && this->value.size() < long1.value.size()) return true;
    if (this->sign != '-' && this->value.size() > long1.value.size()) return true;
    if (this->sign != '-' && this->value.size() < long1.value.size()) return false;
    if (this->sign != '-')
        for (int i=0; i < (int)this->value.size(); ++i)
        {
            if (this->value.at(i) > long1.value.at(i)) return true;
            if (this->value.at(i) < long1.value.at(i)) return false;
        }
    else
        for (int i=0; i < (int)this->value.size(); ++i)
        {
            if (this->value.at(i) < long1.value.at(i)) return true;
            if (this->value.at(i) > long1.value.at(i)) return false;
        }
    return false;
}


bool LongInteger::operator== (const LongInteger& long1) const
{
    if (this->value == LongInteger().value && long1.value == LongInteger().value) return true; //if both of numbers is "0" or "-0"
    if (this->sign != long1.sign) return false;
    if (this->value.size() != long1.value.size()) return false;
    for (int i=0; i < (int)this->value.size(); ++i)
        if (this->value.at(i) != long1.value.at(i)) return false;
    return true;
}


bool LongInteger::operator!=(const LongInteger& long1) const
{
    return (*this == long1) ? false : true;
}
