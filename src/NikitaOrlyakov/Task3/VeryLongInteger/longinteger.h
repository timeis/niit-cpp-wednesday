#ifndef LONGINTEGER_H
#define LONGINTEGER_H
#include <deque>
#include <string>

struct InvalidValue{};
struct DivideByZero{};
struct NegativeInvolution{};

class LongInteger
{
private:
    std::deque<int> value;
    char sign;

public:
    LongInteger();
    LongInteger(const std::string&);

    void delete_zero();
    std::string toString() const;
    int compare_by_module(const LongInteger&) const; // return 1 if first > second, -1 if first < second, 0 if first == second

    LongInteger operator+(const LongInteger&) const;
    LongInteger operator-(LongInteger) const;
    LongInteger operator*(const LongInteger&) const;
    LongInteger operator/(const LongInteger&) const;
    LongInteger operator%(const LongInteger&) const;
    LongInteger operator^(const LongInteger&) const;

    bool operator<(const LongInteger&) const;
    bool operator>(const LongInteger&) const;
    bool operator==(const LongInteger&) const;
    bool operator!=(const LongInteger&) const;
};

#endif // LONGINTEGER_H
