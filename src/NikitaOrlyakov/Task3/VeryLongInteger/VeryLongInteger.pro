#-------------------------------------------------
#
# Project created by QtCreator 2016-05-05T11:23:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VeryLongInteger
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    longinteger.cpp

HEADERS  += mainwindow.h \
    longinteger.h

FORMS    += mainwindow.ui
