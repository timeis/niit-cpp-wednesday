#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include "longinteger.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pb_Plus_clicked();

    void on_pb_Minus_clicked();

    void on_pb_Greater_clicked();

    void on_pb_Less_clicked();

    void on_pb_Equal_clicked();

    void on_pb_NotEqual_clicked();

    void on_pb_Multiply_clicked();

    void on_pb_Divide_clicked();

    void on_pb_Modulo_clicked();

    void on_pb_Involution_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
