#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pb_Plus_clicked()
{
    if (ui->te_Number1->toPlainText().isEmpty() || ui->te_Number2->toPlainText().isEmpty())
        QMessageBox::information(this, "Error", "One of the numbers or both are empty");
    try
    {
        LongInteger long1 = ui->te_Number1->toPlainText().toStdString();
        LongInteger long2 = ui->te_Number2->toPlainText().toStdString();
        ui->te_Answer->setText(QString::fromStdString((long1+long2).toString()));
    }
    catch (InvalidValue)
    {
        QMessageBox::information(this, "Error", "One of the numbers or both are invalid");
    }
}


void MainWindow::on_pb_Minus_clicked()
{
    if (ui->te_Number1->toPlainText().isEmpty() || ui->te_Number2->toPlainText().isEmpty())
        QMessageBox::information(this, "Error", "One of the numbers or both are empty");
    try
    {    if (ui->te_Number1->toPlainText().isEmpty() || ui->te_Number2->toPlainText().isEmpty())
            QMessageBox::information(this, "Error", "One of the numbers or both are empty");
        try
        {
            LongInteger long1 = ui->te_Number1->toPlainText().toStdString();
            LongInteger long2 = ui->te_Number2->toPlainText().toStdString();
            ui->te_Answer->setText(QString::fromStdString((long1+long2).toString()));
        }
        catch (InvalidValue)
        {
            QMessageBox::information(this, "Error", "One of the numbers or both are invalid");
        }
        LongInteger long1 = ui->te_Number1->toPlainText().toStdString();
        LongInteger long2 = ui->te_Number2->toPlainText().toStdString();
        ui->te_Answer->setText(QString::fromStdString((long1-long2).toString()));
    }
    catch (InvalidValue)
    {
        QMessageBox::information(this, "Error", "One of the numbers or both are invalid");
    }
}


void MainWindow::on_pb_Greater_clicked()
{
    if (ui->te_Number1->toPlainText().isEmpty() || ui->te_Number2->toPlainText().isEmpty())
        QMessageBox::information(this, "Error", "One of the numbers or both are empty");
    try
    {
        LongInteger long1 = ui->te_Number1->toPlainText().toStdString();
        LongInteger long2 = ui->te_Number2->toPlainText().toStdString();
        ui->te_Answer->setText((long1>long2) ? "true" : "false");
    }
    catch (InvalidValue)
    {
        QMessageBox::information(this, "Error", "One of the numbers or both are invalid");
    }
}


void MainWindow::on_pb_Less_clicked()
{
    if (ui->te_Number1->toPlainText().isEmpty() || ui->te_Number2->toPlainText().isEmpty())
        QMessageBox::information(this, "Error", "One of the numbers or both are empty");
    try
    {
        LongInteger long1 = ui->te_Number1->toPlainText().toStdString();
        LongInteger long2 = ui->te_Number2->toPlainText().toStdString();
        ui->te_Answer->setText((long1<long2) ? "true" : "false");
    }
    catch (InvalidValue)
    {
        QMessageBox::information(this, "Error", "One of the numbers or both are invalid");
    }
}


void MainWindow::on_pb_Equal_clicked()
{
    if (ui->te_Number1->toPlainText().isEmpty() || ui->te_Number2->toPlainText().isEmpty())
        QMessageBox::information(this, "Error", "One of the numbers or both are empty");
    try
    {
        LongInteger long1 = ui->te_Number1->toPlainText().toStdString();
        LongInteger long2 = ui->te_Number2->toPlainText().toStdString();
        ui->te_Answer->setText((long1==long2) ? "true" : "false");
    }
    catch (InvalidValue)
    {
        QMessageBox::information(this, "Error", "One of the numbers or both are invalid");
    }
}


void MainWindow::on_pb_NotEqual_clicked()
{
    if (ui->te_Number1->toPlainText().isEmpty() || ui->te_Number2->toPlainText().isEmpty())
        QMessageBox::information(this, "Error", "One of the numbers or both are empty");
    try
    {
        LongInteger long1 = ui->te_Number1->toPlainText().toStdString();
        LongInteger long2 = ui->te_Number2->toPlainText().toStdString();
        ui->te_Answer->setText((long1!=long2) ? "true" : "false");
    }
    catch (InvalidValue)
    {
        QMessageBox::information(this, "Error", "One of the numbers or both are invalid");
    }
}


void MainWindow::on_pb_Multiply_clicked()
{
    if (ui->te_Number1->toPlainText().isEmpty() || ui->te_Number2->toPlainText().isEmpty())
        QMessageBox::information(this, "Error", "One of the numbers or both are empty");
    try
    {
        LongInteger long1 = ui->te_Number1->toPlainText().toStdString();
        LongInteger long2 = ui->te_Number2->toPlainText().toStdString();
        ui->te_Answer->setText(QString::fromStdString((long1*long2).toString()));
    }
    catch (InvalidValue)
    {
        QMessageBox::information(this, "Error", "One of the numbers or both are invalid");
    }
}


void MainWindow::on_pb_Divide_clicked()
{
    if (ui->te_Number1->toPlainText().isEmpty() || ui->te_Number2->toPlainText().isEmpty())
        QMessageBox::information(this, "Error", "One of the numbers or both are empty");
    try
    {
        LongInteger long1 = ui->te_Number1->toPlainText().toStdString();
        LongInteger long2 = ui->te_Number2->toPlainText().toStdString();
        ui->te_Answer->setText(QString::fromStdString((long1/long2).toString()));
    }
    catch (InvalidValue)
    {
        QMessageBox::information(this, "Error", "One of the numbers or both are invalid");
    }
    catch (DivideByZero)
    {
        QMessageBox::information(this, "Error", "Divide by zero");
    }
}


void MainWindow::on_pb_Modulo_clicked()
{
    if (ui->te_Number1->toPlainText().isEmpty() || ui->te_Number2->toPlainText().isEmpty())
        QMessageBox::information(this, "Error", "One of the numbers or both are empty");
    try
    {
        LongInteger long1 = ui->te_Number1->toPlainText().toStdString();
        LongInteger long2 = ui->te_Number2->toPlainText().toStdString();
        ui->te_Answer->setText(QString::fromStdString((long1%long2).toString()));
    }
    catch (InvalidValue)
    {
        QMessageBox::information(this, "Error", "One of the numbers or both are invalid");
    }
    catch (DivideByZero)
    {
        QMessageBox::information(this, "Error", "Divide by zero");
    }
}


void MainWindow::on_pb_Involution_clicked()
{
    if (ui->te_Number1->toPlainText().isEmpty() || ui->te_Number2->toPlainText().isEmpty())
        QMessageBox::information(this, "Error", "One of the numbers or both are empty");
    try
    {
        LongInteger long1 = ui->te_Number1->toPlainText().toStdString();
        LongInteger long2 = ui->te_Number2->toPlainText().toStdString();
        ui->te_Answer->setText(QString::fromStdString((long1^long2).toString()));
    }
    catch (InvalidValue)
    {
        QMessageBox::information(this, "Error", "One of the numbers or both are invalid");
    }
    catch (NegativeInvolution)
    {
        QMessageBox::information(this, "Error", "Negative involution");
    }
}
