#ifndef PREY_H
#define PREY_H
#include "cell.h"


class Prey : public Cell
{
protected:
    int timeToReproduce;

    void moveFrom(Coordinate,Coordinate);
    virtual Cell* reproduce(Coordinate);

public:
    Prey(const Coordinate&);
    virtual ~Prey() {}

    virtual void process();

};

#endif // PREY_H
