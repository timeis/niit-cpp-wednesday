#ifndef CELL_H
#define CELL_H
#include "ocean.h"

class Cell
{
    friend class Ocean;

protected:
    static Ocean* ocean;
    Coordinate offset;
    char image;

    Cell* getCellAt(Coordinate);
    void assignCellAt(Coordinate, Cell*);
    Cell* getNeighborWithImage(const char&);
    Coordinate getEmptyNeighborCoord();
    Coordinate getPreyNeighborCoord();
    Cell* north();
    Cell* south();
    Cell* east();
    Cell* west();

    virtual Cell* reproduce(Coordinate);

public:
    Cell(const Coordinate&);
    Cell() {}
    virtual ~Cell() {}

    const Coordinate& getOffset() const;
    void setOffset(Coordinate);
    char getImage() const;

    void display() const;
    virtual void process() {}
};

#endif // CELL_H
