#include "ocean.h"
#include "obstacle.h"
#include "cell.h"
#include "predator.h"
#include "prey.h"
#include <time.h>
#include <stdlib.h>


Ocean* Cell::ocean;


void Ocean::initialize()
{
    srand(time(0));

    this->numRows = MaxRows;
    this->numColumns = MaxCols;
    this->oceanSize = numRows * numColumns;
    this->numObstacles = DefaultNumObstacles;
    this->numPredators = DefaultNumPredators;
    this->numPrey = DefaultNumPrey;

    this->initCells();
}


void Ocean::initCells()
{
    this->addEmptyCells();

    // here may be manual keyboard input number of Obstacles, Prey and Predator

    this->addObstacles();
    this->addPredator();
    this->addPrey();
//    this->displayBorder();
//    this->displayCells();
//    this->displayBorder();
//    this->displayStats(-1);
    Cell::ocean = this;
}


void Ocean::addEmptyCells()
{
    for (unsigned int row = 0; row < this->numRows; ++row)
        for (unsigned int col = 0; col < this->numColumns; ++col)
            this->cells[row][col] = new Cell(Coordinate(col,row));
}


void Ocean::addObstacles()
{
    Coordinate coord;
    for (unsigned int count = 0; count < this->numObstacles; ++count)
    {
        coord = this->getEmptyCellCoord();
        this->cells[coord.getY()][coord.getX()] = new Obstacle(coord);
    }
}


void Ocean::addPredator()
{
    Coordinate coord;
    for (unsigned int count = 0; count < this->numPredators; ++count)
    {
        coord = this->getEmptyCellCoord();
        this->cells[coord.getY()][coord.getX()] = new Predator(coord);
    }
}


void Ocean::addPrey()
{
    Coordinate coord;
    for (unsigned int count = 0; count < this->numPrey; ++count)
    {
        coord = this->getEmptyCellCoord();
        this->cells[coord.getY()][coord.getX()] = new Prey(coord);
    }
}


Coordinate Ocean::getEmptyCellCoord()
{
    unsigned int x,y;
    Coordinate coord;

    do
    {
        x = rand()%this->numColumns;
        y = rand()%this->numRows;
    } while (this->cells[y][x]->getImage() != DefaultImage);

    coord = cells[y][x]->getOffset();
    delete cells[y][x];
    return coord;
}


void Ocean::displayBorder() const
{
    for (unsigned int col = 0; col < this->numColumns; ++col)
        std::cout << "*";
    std::cout << std::endl;
}


void Ocean::displayCells() const
{
    for (unsigned int row = 0; row < this->numRows; ++row)
    {
        for (unsigned int col = 0; col < this->numColumns; ++col)
            this->cells[row][col]->display();
        std::cout << std::endl;
    }
}


void Ocean::displayStats(const int& iter) const
{
    std::cout << std::endl << std::endl;
    std::cout << "Iteration number: " << (iter+1);
    std::cout << " Predator: " << this->numPredators;
    std::cout << " Prey: " << this->numPrey << std::endl;
}


unsigned int Ocean::getNumPrey() const {return this->numPrey;}


unsigned int Ocean::getNumPredators() const {return this->numPredators;}


void Ocean::setNumPrey(const unsigned int& num) {this->numPrey = num;}


void Ocean::setNumPredators(const unsigned int& num) {this->numPredators = num;}


int Ocean::run()
{
//    unsigned int numIter = DefaultNumIterations;

    // here may be manual keyboard input number of iteration

//    for (unsigned int iter= 0; iter < numIter; ++iter)
//    {
//        system("clear");
        if(this->numPredators == 0 && this->numPrey == 0) return 0;//break;
        for (unsigned int row = 0; row < this->numRows; ++row)
            for (unsigned int col = 0; col < this->numColumns; ++col)
                this->cells[row][col]->process();
        return 1;
//        this->displayBorder();
//        this->displayCells();
//        this->displayBorder();
//        this->displayStats(iter);
//        std::cout.flush();

//        clock_t now = clock();
//        while (clock() < now + CLOCKS_PER_SEC/5);

//    }
}
