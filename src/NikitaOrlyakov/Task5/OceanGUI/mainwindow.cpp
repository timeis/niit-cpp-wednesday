#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QBrush>
#include "cell.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(fillTable()));

    myocean = new Ocean;
    myocean->initialize();

    ui->tableView->resize(MaxCols*10+2, MaxRows*10+2);
    model = new QStandardItemModel(MaxRows,MaxCols);
    ui->tableView->setModel(model);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbStart_clicked()
{
    this->timer->start(200);

}


void MainWindow::fillTable()
{
    for (unsigned int row = 0; row < this->myocean->numRows; ++row)
        for (unsigned int col = 0; col < this->myocean->numColumns; ++col)
        {
            switch(this->myocean->cells[row][col]->getImage())
            {
            case '-':
            {
                QStandardItem* emptycell = new QStandardItem;
                emptycell->setBackground(QBrush(QColor(255,255,255)));
                model->setItem(row,col,emptycell);
                break;
            }
            case 'f':
            {
                QStandardItem* prey = new QStandardItem;
                prey->setBackground(QBrush(QColor(150,250,150)));
                model->setItem(row,col,prey);
                break;
            }
            case 'S':
            {
                QStandardItem* predator = new QStandardItem;
                predator->setBackground(QBrush(QColor(250,150,150)));
                model->setItem(row,col,predator);
                break;
            }
            case '#':
            {
                QStandardItem* obstacle = new QStandardItem;
                obstacle->setBackground(QBrush(QColor(100,100,100)));
                model->setItem(row,col,obstacle);
                break;
            }
            }
        }
    if (myocean->run() == 0) this->timer->stop();
}
