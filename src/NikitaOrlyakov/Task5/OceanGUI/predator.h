#ifndef PREDATOR_H
#define PREDATOR_H
#include "prey.h"

class Predator: public Prey
{
protected:
    unsigned int timeToFeed;
    virtual Cell* reproduce(Coordinate);

public:
    Predator(const Coordinate&);
    virtual ~Predator() {}

    virtual void process();
};

#endif // PREDATOR_H
