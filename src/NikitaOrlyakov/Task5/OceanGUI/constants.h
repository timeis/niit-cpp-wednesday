#ifndef CONSTANTS_H
#define CONSTANTS_H


const unsigned int MaxRows = 50;//25
const unsigned int MaxCols = 70;//70

const unsigned int DefaultNumObstacles = 150;//75
const unsigned int DefaultNumPredators = 40;//20
const unsigned int DefaultNumPrey = 300;//150
const unsigned int DefaultNumIterations = 1000;

const char DefaultImage = '-';
const char DefaultPreyImage = 'f';
const char DefaultPredImage = 'S';
const char ObstacleImage = '#';

const unsigned int TimeToFeed = 6;//6
const unsigned int TimeToReproduce = 6;//6


#endif // CONSTANTS_H
