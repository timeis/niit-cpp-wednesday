#include "prey.h"


Prey::Prey(const Coordinate& coord):Cell(coord)
{
    this->timeToReproduce = TimeToReproduce;
    this->image = DefaultPreyImage;
}


void Prey::moveFrom(Coordinate fromcoord,Coordinate tocoord)
{
    Cell* tocell;
    --(this->timeToReproduce);

    if (tocoord != fromcoord)
    {
        tocell = getCellAt(tocoord);
        delete tocell;
        setOffset(tocoord);
        assignCellAt(tocoord,this);
        if(this->timeToReproduce <= 0)
        {
            this->timeToReproduce = TimeToReproduce;
            assignCellAt(fromcoord, this->reproduce(fromcoord));
        }
        else
            assignCellAt(fromcoord, new Cell(fromcoord));
    }
}


Cell* Prey::reproduce(Coordinate coord)
{
    Prey* temp = new Prey(coord);
    this->ocean->setNumPrey(this->ocean->getNumPrey()+1);
    return (Cell*) temp;
}


void Prey::process()
{
    Coordinate tocoord;
    tocoord = this->getEmptyNeighborCoord();
    this->moveFrom(this->offset,tocoord);
}
