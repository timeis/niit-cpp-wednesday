#ifndef COORDINATE_H
#define COORDINATE_H

class Coordinate
{
private:
    unsigned int x;
    unsigned int y;

public:
    Coordinate(const unsigned int&,const unsigned int&);
    Coordinate();
    Coordinate(const Coordinate&);
    ~Coordinate(){}

    unsigned int getX();
    unsigned int getY();
    void setX(const unsigned int&);
    void setY(const unsigned int&);

    void operator=(const Coordinate&);
    bool operator==(const Coordinate&) const;
    bool operator!=(const Coordinate&) const;
};

#endif // COORDINATE_H
