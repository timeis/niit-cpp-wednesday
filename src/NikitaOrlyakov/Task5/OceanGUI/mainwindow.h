#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ocean.h"
#include <QTimer>
#include <QStandardItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbStart_clicked();
    void fillTable();

private:
    Ui::MainWindow *ui;
    QTimer *timer;
    Ocean* myocean;
    QStandardItemModel* model;
};

#endif // MAINWINDOW_H
