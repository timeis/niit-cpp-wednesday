#include "cell.h"
#include "ocean.h"
#include <time.h>
#include <stdlib.h>


Cell::Cell(const Coordinate& coord):offset(coord)
{
    this->image = DefaultImage;
}


Cell* Cell::getCellAt(Coordinate coord)
{
    return this->ocean->cells[coord.getY()][coord.getX()];
}


void Cell::assignCellAt(Coordinate coord, Cell* cell)
{
    this->ocean->cells[coord.getY()][coord.getX()] = cell;
}


Cell* Cell::getNeighborWithImage(const char& im)
{
    Cell* neighbors[4];
    unsigned int i = 0;

    if(this->north()->getImage() == im) neighbors[i++] = north();
    if(this->south()->getImage() == im) neighbors[i++] = south();
    if(this->east()->getImage() == im) neighbors[i++] = east();
    if(this->west()->getImage() == im) neighbors[i++] = west();

    if (i == 0) return this;
    if (i == 1) return neighbors[0];
    else return neighbors[rand()%(i-1)];
}


Coordinate Cell::getEmptyNeighborCoord()
{
    return this->getNeighborWithImage(DefaultImage)->getOffset();
}


Coordinate Cell::getPreyNeighborCoord()
{
    return this->getNeighborWithImage(DefaultPreyImage)->getOffset();
}


Cell* Cell::north()
{
    unsigned int y;

    y = (this->offset.getY() > 0) ? (this->offset.getY()-1):(this->ocean->numRows-1);
    return this->ocean->cells[y][this->offset.getX()];
}


Cell* Cell::south()
{
    unsigned int y;

    y = (this->offset.getY()+1)%this->ocean->numRows;
    return this->ocean->cells[y][this->offset.getX()];
}


Cell* Cell::east()
{
    unsigned int x;

    x = (this->offset.getX()+1)%this->ocean->numColumns;
    return this->ocean->cells[this->offset.getY()][x];

}


Cell* Cell::west()
{
    unsigned int x;

    x = (this->offset.getX() > 0) ? (this->offset.getX()-1):(this->ocean->numColumns-1);
    return this->ocean->cells[this->offset.getY()][x];
}


Cell* Cell::reproduce(Coordinate coord) // WHY FOR?
{
    Cell* temp = new Cell(coord);
    return temp;
}


const Coordinate& Cell::getOffset() const {return this->offset;}


void Cell::setOffset(Coordinate coord) {this->offset = coord;}


char Cell::getImage() const {return this->image;}


void Cell::display() const
{
    std::cout << this->image; //WHY like this?
}

