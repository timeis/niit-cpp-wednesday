#-------------------------------------------------
#
# Project created by QtCreator 2016-05-26T14:53:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OceanGUI
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cell.cpp \
    coordinate.cpp \
    ocean.cpp \
    predator.cpp \
    prey.cpp

HEADERS  += mainwindow.h \
    cell.h \
    constants.h \
    coordinate.h \
    obstacle.h \
    ocean.h \
    predator.h \
    prey.h

FORMS    += mainwindow.ui
