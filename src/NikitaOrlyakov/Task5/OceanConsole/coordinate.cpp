#include "coordinate.h"


Coordinate::Coordinate(const unsigned int& X,const unsigned int& Y):x(X),y(Y){}


Coordinate::Coordinate()
{
    this->x = 0;
    this->y = 0;
}


Coordinate::Coordinate(const Coordinate& coord)
{
    this->x = coord.x;
    this->y = coord.y;
}


unsigned int Coordinate::getX(){return this->x;}


unsigned int Coordinate::getY(){return this->y;}


void Coordinate::setX(const unsigned int& X) {this->x = X;}


void Coordinate::setY(const unsigned int& Y) {this->y = Y;}


void Coordinate::operator=(const Coordinate& coord)
{
    this->x = coord.x;
    this->y = coord.y;
}


bool Coordinate::operator==(const Coordinate& coord) const
{
    return (this->x == coord.x && this->y == coord.y);
}


bool Coordinate::operator!=(const Coordinate& coord) const
{
    return (this->x != coord.x || this->y != coord.y);
}
