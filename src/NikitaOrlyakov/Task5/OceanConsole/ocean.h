#ifndef OCEAN_H
#define OCEAN_H
#include <iostream>
#include "constants.h"
#include "coordinate.h"

class Cell;


class Ocean
{
    friend class Cell;

private:
    unsigned int numRows;
    unsigned int numColumns;
    unsigned int oceanSize;
    unsigned int numPrey;
    unsigned int numPredators;
    unsigned int numObstacles;
    Cell* cells[MaxRows][MaxCols];

    void initCells();
    void addEmptyCells();
    void addObstacles();
    void addPredator();
    void addPrey();
    Coordinate getEmptyCellCoord();

    void displayBorder() const;
    void displayCells() const;
    void displayStats(const int&) const;

public:
    unsigned int getNumPrey() const;
    unsigned int getNumPredators() const;
    void setNumPrey(const unsigned int&);
    void setNumPredators(const unsigned int&);

    void initialize();

    void run();
};

#endif // OCEAN_H
