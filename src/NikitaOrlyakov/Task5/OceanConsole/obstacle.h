#ifndef OBSTACLE_H
#define OBSTACLE_H
#include "cell.h" //check this  - in book Predator.h

class Obstacle: public Cell
{
public:
    Obstacle(const Coordinate& coord):Cell(coord)
    {
        this->image = ObstacleImage;
    }
    virtual ~Obstacle() {}
};

#endif // OBSTACLE_H
