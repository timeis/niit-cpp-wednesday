#include "ocean.h"


int main()
{
    Ocean* myocean = new Ocean;
    myocean->initialize();
    myocean->run();
    return 0;
}
