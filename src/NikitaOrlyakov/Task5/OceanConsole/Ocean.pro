TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    ocean.cpp \
    cell.cpp \
    coordinate.cpp \
    predator.cpp \
    prey.cpp

HEADERS += \
    cell.h \
    constants.h \
    coordinate.h \
    obstacle.h \
    ocean.h \
    predator.h \
    prey.h
