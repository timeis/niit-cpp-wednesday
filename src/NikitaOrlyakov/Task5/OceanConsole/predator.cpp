#include "predator.h"


Predator::Predator(const Coordinate& coord):Prey(coord)
{
    this->timeToFeed = TimeToFeed;
    this->image = DefaultPredImage;
}


Cell* Predator::reproduce(Coordinate coord)
{
    Predator* temp = new Predator(coord);
    this->ocean->setNumPredators(this->ocean->getNumPredators()+1);
    return (Cell*) temp;
}


void Predator::process()
{
    Coordinate tocoord;
    if (--(this->timeToFeed) <= 0)
    {
        assignCellAt(this->offset, new Cell(this->offset));
        this->ocean->setNumPredators(this->ocean->getNumPredators()-1);
        delete this;
    }
    else
    {
        tocoord = getPreyNeighborCoord();
        if (tocoord != this->offset)
        {
            this->ocean->setNumPrey(this->ocean->getNumPrey()-1);
            this->timeToFeed = TimeToFeed;
            moveFrom(this->offset, tocoord);
        }
        else
        {
            tocoord = this->getEmptyNeighborCoord();
            this->moveFrom(this->offset, tocoord);
        }
    }
}

